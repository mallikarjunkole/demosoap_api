package in.bank.application.serviceImpl;

import javax.jws.WebService;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import in.bank.application.customException.CustomException;
import in.bank.application.customException.ErrorMessages;
import in.bank.application.dao.BankDao;
import in.bank.application.entity.AccountDetails;
import in.bank.application.resource.HibernateUtil;
import in.bank.application.service.BankService;

@WebService(endpointInterface = "in.bank.application.service.BankService")
public class BankServiceImpl implements BankService 
{
	SessionFactory sf = HibernateUtil.getSessionFactory();
	Session session = sf.openSession();
	@Override
	public String addBankUserDetails(String firstname, String lastname, String accounType, int accountBalance) 
	{
		System.out.println("First Check");
		String Msg="";
		Transaction t = session.beginTransaction();
		AccountDetails account = new AccountDetails();
		account.setFirstName(firstname);
		account.setLastName(lastname);
		account.setAccountType(accounType);
		System.out.println("Seocnd Check");
		account.setAccountBalance(accountBalance);
		session.save(account);
		
		System.out.println("Updated Id : "+account.getUserId());
		if(account.getUserId() > 0)
		{
			Msg = "Applicant Details Successfully Inserted for UserId : "+account.getUserId()+".Check Your Full Name :"+account.getFirstName()+" "+account.getLastName();
		}else
		{
			Msg = "Unable to save Application Details. ";
		}
		
		t.commit();
		session.close();
		return Msg;
	}

	@Override
	public AccountDetails getBankAccountHolderDetailsByID(int UserID) {
		AccountDetails account = null;
		session.beginTransaction();
		try {
			account = (AccountDetails) session.load(AccountDetails.class, Long.valueOf(UserID));
			System.out.println("Test : " + account.getUserId());
			System.out.println("Test : " + account.getFirstName());
			System.out.println("Test : " + account.getLastName());
			return account;
		} finally {
			session.close();
		}
	}

	@Override
	public String withdrawnAmount(int UserId, int WithdrawnAmount) 
	{
		String Status = "";
		BankDao bd = new BankDao();
		int flag = 0;
		try
		{
			flag = bd.doWithdrawnOpertaion(UserId, WithdrawnAmount);
		} catch (CustomException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Status = e.getMessage();
			System.out.println("Error Message :" + e.getMessage());
		}
		if (flag == 1) 
		{
			Status = "Withdrawn Transaction Successful";
			System.out.println("Withdrawn Transaction Successful");

		}
		return Status;
	}

}
