package in.bank.application.service;

import javax.jws.WebMethod;
import javax.jws.WebService;

import in.bank.application.entity.AccountDetails;

@WebService
public interface BankService
{
	@WebMethod
	public String addBankUserDetails(String firstname, String lastname, String accounType, int accountBalance );
	
	@WebMethod
	public AccountDetails getBankAccountHolderDetailsByID(int UserID);
	
	@WebMethod
	public String withdrawnAmount(int UserId, int WithdrawnAmount);
}
