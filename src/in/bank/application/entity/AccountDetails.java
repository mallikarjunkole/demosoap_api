package in.bank.application.entity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name = "account_details")
public class AccountDetails implements Serializable
{ 
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private long UserId;

	@Column(name = "first_name")
	private String FirstName;

	@Column(name = "last_name")
	private String LastName;
	
	@Column(name = "account_type")
	private String AccountType;

	@Column(name = "account_balance")
	private int AccountBalance;
	
	 	
	public AccountDetails(){}
	
	public AccountDetails(String firstname, String lastname, String accounType, int accountBalance)
	{
		this.FirstName = firstname;
		this.LastName = lastname;
		this.AccountType = accounType;
		this.AccountBalance = accountBalance;
	}

	public long getUserId() {
		return UserId;
	}

	public void setUserId(long userId) {
		UserId = userId;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getAccountType() {
		return AccountType;
	}

	public void setAccountType(String accountType) {
		AccountType = accountType;
	}

	public int getAccountBalance() {
		return AccountBalance;
	}

	public void setAccountBalance(int accountBalance) {
		AccountBalance = accountBalance;
	}

}
