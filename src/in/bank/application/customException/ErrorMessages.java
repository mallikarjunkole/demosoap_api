package in.bank.application.customException;

public class ErrorMessages 
{
	public static final String InsufficientBalance="You can't make this transaction due to Insufficient Balance. ";
	public static final String OverdrawnBalanceRequest="Amount you want to withdrawn from account is greater than current balance. ";
	public static final String InvalidAccountDetails="Invalid Account Details. ";
	public static final String TransactionFailed="Transaction Failed due to invalid Input. ";
	
}
