package in.bank.application.customException;

public class CustomException extends RuntimeException
{
	public CustomException(String msg)
	{
		super(msg);
	}
}
