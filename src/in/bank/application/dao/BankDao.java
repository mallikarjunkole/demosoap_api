package in.bank.application.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import in.bank.application.customException.ErrorMessages;
import in.bank.application.customException.CustomException;
import in.bank.application.entity.AccountDetails;
import in.bank.application.resource.HibernateUtil;

public class BankDao 
{
	SessionFactory sf = HibernateUtil.getSessionFactory();
	Session session = sf.openSession();

	public int doWithdrawnOpertaion(int userId, int withdrawnAmount) throws CustomException
	{
		int flag = 1;
		String msg="";
		Query q = session.createQuery("from AccountDetails where UserId =" + userId);
		List<AccountDetails> listt = q.getResultList();
		if (listt.size() > 0) 
		{
			for (int i = 0; i < listt.size(); i++) 
			{
				flag = doWithdrawn(userId, withdrawnAmount, listt.get(i).getAccountBalance());
				// flag = 1;
			}
		}
		else
		{
			flag = 2;
			throw new CustomException(ErrorMessages.InvalidAccountDetails+"for User ID"+userId+". This userId is not available in system.");
		}

		session.close();
		return flag;

	}

	private int doWithdrawn(int userId, int withdrawnAmount, int accBal) throws CustomException 
	{
		int flag = 0;
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		Transaction t = session.beginTransaction();
		
			if (withdrawnAmount < accBal) 
			{
				accBal = accBal - withdrawnAmount;
				System.out.println("Transaction Successful with Account Number " + userId + "....Current Balance :" + accBal);
				flag = 1;
			} 
			else 
			{
				flag = 3;
				throw new CustomException(ErrorMessages.OverdrawnBalanceRequest+" for User ID "+userId);
				
			}
		
		AccountDetails user = session.get(AccountDetails.class, Long.valueOf(userId)); 
		user.setAccountBalance((accBal));
		session.update(user);
		t.commit();
		session.close();
		System.out.println();
		return flag;
	}

}
